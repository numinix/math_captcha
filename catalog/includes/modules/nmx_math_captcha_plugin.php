<?php

  // validate if it is human interacting
  $antibot_subscribe = zen_db_prepare_input($_POST['antibot_subscribe']);
  if ($antibot_subscribe != $_SESSION['antibot_subscribe']) {
    $error = true;

    unset($_SESSION['antibot_subscribe']);
    unset($_SESSION['antibot_subscribe1']);
    unset($_SESSION['antibot_subscribe2']);

    $main_page = $_GET['main_page'];

    switch ($main_page) {
      case 'contact_us':
        $messageStack->add('contact', ANTIBOT_ERROR_MESSAGE, 'error');
        break;
      case 'create_account':
        $messageStack->add('create_account', ANTIBOT_ERROR_MESSAGE, 'error');
        break; 
      default:
        break;
    }
  }
